from PIL import Image, ImageDraw


class Makeup_artist(object):
    def __init__(self):
        pass

    def apply_makeup(self, img):
        return img.transpose(Image.FLIP_LEFT_RIGHT)

    def notBarcode(self, message):
        img = Image.new('RGB', (300, 150), color = "white")
        d = ImageDraw.Draw(img)
        d.text((100,70), message, fill=(73, 109, 137))
        return img

    def detailBarcode(self, detailBC):
        img = Image.new('RGB', (300, 150), color = "black")
        d = ImageDraw.Draw(img)
        d.text((100,70), detailBC, fill=(255, 255, 255))
        return img
